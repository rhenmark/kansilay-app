import React from "react";
import { View } from "react-native";
import {
  createBottomTabNavigator,
  createStackNavigator,
  createAppContainer,
  NavigationInjectedProps,
  TabBarIconProps
} from "react-navigation";
import { MaterialCommunityIcons as Icon } from "@expo/vector-icons";

import { Home, Events, Timeline, Favorites, More } from "../screens";

const getIconName = (routeName: string) => {
  let iconName = "home-variant-outline";

  switch (routeName) {
    case "Home":
      iconName = "home-variant-outline";
      break;

    case "Events":
      iconName = "calendar";
      break;

    case "Timeline":
      iconName = "image-filter-center-focus";
      break;

    case "Favorites":
      iconName = "bookmark-outline";
      break;

    case "More":
      iconName = "menu";
      break;

    default:
      iconName = "home-variant-outline";
  }

  return iconName;
};

const navigationOptions = (isTimeline?: boolean) => ({
  navigationOptions: () => ({
    headerTitle: "KanSilay App",
    headerStyle: {
      backgroundColor: "#000",
      borderColor: "#000"
    },
    headerTintColor: "#fff",
    headerRight: (
      <View style={{ paddingRight: 10 }}>
        <Icon
          name={isTimeline ? "camera-outline" : "magnify"}
          color="#fff"
          size={24}
        />
      </View>
    )
  })
});

const HomeNavigator = createStackNavigator({
  Home: {
    screen: Home,
    ...navigationOptions()
  }
});

const EventsNavigator = createStackNavigator({
  Events: {
    screen: Events,
    ...navigationOptions()
  }
});

const TimelineNavigator = createStackNavigator({
  Timeline: {
    screen: Timeline,
    ...navigationOptions(true)
  }
});

const BottomNav = createBottomTabNavigator(
  {
    Home: HomeNavigator,
    Events: EventsNavigator,
    Timeline: TimelineNavigator,
    Favorites,
    More: More
  },
  {
    defaultNavigationOptions: ({ navigation }: NavigationInjectedProps) => ({
      tabBarIcon: ({ tintColor, focused }: TabBarIconProps) => {
        const { routeName } = navigation.state;
        let iconName = getIconName(routeName);
        return (
          <Icon
            name={iconName}
            size={focused ? 44 : 24}
            color={`${tintColor}`}
          />
        );
      }
    }),
    tabBarOptions: {
      showLabel: false,
      labelStyle: {
        fontSize: 14
        // fontWeight: "bold"
      }
    }
  }
);

const AppNavigator = createAppContainer(BottomNav);

export default AppNavigator;

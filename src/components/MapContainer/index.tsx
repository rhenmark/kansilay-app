import React from "react";
import { StyleSheet } from "react-native";
import MapView from "react-native-maps";
import { ISafeProps } from "../../Iprops/components/safecontainer";

const MapContainer = ({ children }: ISafeProps) => (
  <MapView style={styles.container}>{children}</MapView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default MapContainer;

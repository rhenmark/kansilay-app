import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { ISafeProps } from "../../Iprops/components/safecontainer";

const SafeContainer = ({ children }: ISafeProps) => (
  <SafeAreaView style={styles.scene}>{children}</SafeAreaView>
);

const styles = StyleSheet.create({
  scene: {
    flex: 1
  }
});

export default SafeContainer;

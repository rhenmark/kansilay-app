import { ReactElement } from "react";

export interface ISafeProps {
  children?: ReactElement;
}

import React from "react";
import { View, Text } from "react-native";
import { SafeContainer } from "../../components";
import HomeTabs from "./screens";

const Home = () => (
  <SafeContainer>
    <Text>Test</Text>
  </SafeContainer>
);

export default HomeTabs;

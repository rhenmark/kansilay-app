import React, { Fragment } from "react";
import { Text, View, ImageBackground } from "react-native";
import Carousel from "react-native-snap-carousel";

import { SafeContainer, MapContainer } from "../../../../components";

const ENTRIES1 = [
  {
    title: "Beautiful and dramatic Antelope Canyon",
    subtitle: "Lorem ipsum dolor sit amet et nuncat mergitur",
    illustration: "https://i.imgur.com/UYiroysl.jpg"
  },
  {
    title: "Earlier this morning, NYC",
    subtitle: "Lorem ipsum dolor sit amet",
    illustration: "https://i.imgur.com/UPrs1EWl.jpg"
  },
  {
    title: "White Pocket Sunset",
    subtitle: "Lorem ipsum dolor sit amet et nuncat ",
    illustration: "https://i.imgur.com/MABUbpDl.jpg"
  },
  {
    title: "Acrocorinth, Greece",
    subtitle: "Lorem ipsum dolor sit amet et nuncat mergitur",
    illustration: "https://i.imgur.com/KZsmUi2l.jpg"
  },
  {
    title: "The lone tree, majestic landscape of New Zealand",
    subtitle: "Lorem ipsum dolor sit amet",
    illustration: "https://i.imgur.com/2nCt3Sbl.jpg"
  },
  {
    title: "Middle Earth, Germany",
    subtitle: "Lorem ipsum dolor sit amet",
    illustration: "https://i.imgur.com/lceHsT6l.jpg"
  }
];

const HeritageHouse = () => {
  const renderItem = ({ item, index }) => {
    return (
      <View>
        <ImageBackground
          source={{ uri: item.illustration }}
          style={{ width: "100%", height: 150 }}
          key={index}
        >
          <Text>{item.title}</Text>
        </ImageBackground>
      </View>
    );
  };

  return (
    <SafeContainer>
      <Fragment>
        <MapContainer />
        <View style={{ position: "absolute", bottom: 20, left: 0, right: 0 }}>
          <Carousel
            data={ENTRIES1}
            renderItem={props => renderItem(props)}
            sliderWidth={600}
            itemWidth={150}
            loop={true}
          />
        </View>
      </Fragment>
    </SafeContainer>
  );
};

export default HeritageHouse;

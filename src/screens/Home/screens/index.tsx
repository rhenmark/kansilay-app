import React, { useState } from "react";
import { StyleSheet } from "react-native";
import {
  TabView,
  SceneMap,
  TabBar
  //   SceneRendererProps
} from "react-native-tab-view";

import HeritageHouses from "./HeritageHouses";
import Foods from "./Foods";
import Places from "./Places";
import { SafeContainer } from "../../../components";

const initialState = {
  index: 0,
  routes: [
    { key: "first", title: "Destinations" },
    { key: "second", title: "Foods" },
    { key: "third", title: "Places" }
  ]
};

const HomeTabs = () => {
  const [state, setstate] = useState(initialState);
  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      scrollEnabled
      style={styles.tabContainer}
      indicatorStyle={styles.tabIndicator}
    />
  );
  return (
    <SafeContainer>
      <TabView
        navigationState={state}
        onIndexChange={index =>
          setstate({
            ...initialState,
            index
          })
        }
        renderScene={SceneMap({
          first: HeritageHouses,
          second: Places,
          third: Foods
        })}
        renderTabBar={renderTabBar}
      />
    </SafeContainer>
  );
};

const styles = StyleSheet.create({
  tabContainer: {
    backgroundColor: "#000"
  },
  tabIndicator: {
    backgroundColor: "#FF974B"
  }
});

export default HomeTabs;
